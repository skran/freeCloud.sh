# freeMyCloud.sh
### A script to turn your computer into a Cloud Server!
### Warning: Run it at your own risk!
### Requirements:
- Operating System: Debian (or derivatives)
### TOdo
- Identifying whether programs are installed already (like ufw, snap)
- Use cron jobs, to automate https certification with certbot
- Support for other GNU-Linux distros (Redhat and Arch)
- Proper Error handling
- A Help menu and Argument handling