#!/bin/bash
if [ `uname -o` != GNU/Linux ]; then
    echo "This Script is only for Debian based GNU/Linux Operating Systems"
    exit
fi
stars='*****************************************\n'

msg="Nextcloud Installation Script\n\n
1) Install Nextcloud\n
2) Configure Nextcloud\n
3) Uninstall Nextcloud\n
4) Exit\nYour Selection:"

prin="echo -e $msg"
$prin && read input
case $input in
    1)
	printf '\nInstalling Nextcloud\n'

printf '\nUpdating your System... \n' && sleep 1 && printf $stars
sudo apt update && sudo apt upgrade -y
printf '\nInstalling snap \n' && sleep 1 && printf $stars
sudo apt -y install snapd
printf '\nInstalling snap package of Nextcloud\n' && sleep 1 && printf $stars
sudo snap install nextcloud
printf '\nNextcloud is successfully installed\n'
echo 'You can access it from http://localhost' && printf $stars
echo Please provide Admin credintials in the following page
sleep 3
xdg-open http://localhost;;
    2) 
	echo Configuring Nextcloud...
	sleep 3
	read -p "Enter the domain or IPs to allow connections from 
	(Enter * to allow all connections) : " domain
	sudo nextcloud.occ config:system:set trusted_domains 1 -value=$domain
 	echo Configuring Firewall...
	sleep 3
	sudo ufw allow 80,443/tcp;;

	3)
		echo Uninstalling Nextcloud...
		sleep 1
		sudo snap remove nextcloud
	echo Reconfiguring Firewall...
	sleep 1
	sudo ufw deny 80,443/tcp;
	;;
    4) exit;;
    *) clear && $prin && read input;;
esac
echo Done
